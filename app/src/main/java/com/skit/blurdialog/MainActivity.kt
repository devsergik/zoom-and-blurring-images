package com.skit.blurdialog

import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.skit.blurdialog.blurUtils.BlurDialog

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<ImageView>(R.id.imageView).setOnClickListener {
            val alertDialog = BlurDialog()
            val transaction = supportFragmentManager.beginTransaction()
            alertDialog.show(transaction, BlurDialog::class.java.getSimpleName())
        }
    }
}